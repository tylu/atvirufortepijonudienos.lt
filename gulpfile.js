'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserify = require('gulp-browserify');
var rename = require("gulp-rename");
var nunjucksRender = require('gulp-nunjucks-render');
var data = require('gulp-data'); // todo
var server = require('gulp-server-livereload');

gulp.task('sass', function () {
    return gulp.src('./src/sass/index.scss')
        .pipe(sourcemaps.init())
        .pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(sourcemaps.write())
        .pipe(rename('styles.css'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('js', function () {
    return gulp.src('./src/js/index.js')
        .pipe(browserify())
        .pipe(rename('bundle.js'))
        .pipe(gulp.dest('./dist'));
});

gulp.task('nunjucks', function() {
    return gulp.src('src/templates/index.nunjucks')
        // Adding data to Nunjucks
        .pipe(data(function() {
            return require('./src/data.json')
        }))
        // Renders template with nunjucks
        .pipe(nunjucksRender({
            path: ['src/templates']
        }))
        // output files in app folder
        .pipe(gulp.dest('dist'))
});

gulp.task('webserver', function() {
    gulp.src('dist')
        .pipe(server({
            livereload: true,
            directoryListing: false,
            open: true
        }));
});

gulp.task('watch', function () {
    gulp.watch('./src/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['js']);
    gulp.watch('./src/templates/**/*.+(html|nunjucks)', ['nunjucks']);
});

gulp.task('build', ['sass', 'nunjucks', 'js']);

gulp.task('default', ['watch', 'build', 'webserver']);