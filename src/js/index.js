function handleItemState(item, activeClass, clickClass) {
    // $(item).addClass(activeClass); // for debugging
    $(item).hover();
    $(item).find(clickClass).click(function() {
        $(item).addClass(activeClass);
    });
    $(item).mouseleave(function() {
        $(item).removeClass(activeClass);
    })
}

$(document).ready(function() {
    $('.artist').each(function(i, artist) {
        handleItemState(artist, 'artist--active', '.artist__hover');
    });

    $('.featured-concert').each(function(i, artist) {
        handleItemState(artist, 'featured-concert--active', '.featured-concert__hover');
    });
});
